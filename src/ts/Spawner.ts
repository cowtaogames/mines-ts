import { AssetManager } from './AssetManager';
import { Ship } from './Ship';
// import { Mine } from './Mine';
// import { ShooterMine } from './ShooterMine';
// import { ChaserMine } from './ChaserMine';
// import { ChaserShooterMine } from './ChaserShooterMine';

import { MineSize, MineType, SpawnState } from './lib';
import { MineBase } from './MineBase';
import { DrifterMine } from './DrifterMine';

export class Spawner {
  state: SpawnState;
  item: paper.Item;

  constructor(pos: paper.Point) {
    this.state = SpawnState.IDLE;
    this.item = AssetManager.spawner();
    this.item.position = pos;
    this.item.visible = true;
  }

  spawn(type: MineType, size: MineSize, ship: Ship): MineBase {
    console.log('spawner spawn', type, size);
    this.state = SpawnState.SPAWNING;
    this.item.visible = false;
    let mine = null;
    switch (type) {
      //   case MineType.CHASER_SHOOTER:
      //     mine = new ChaserShooterMine(size, this.item.position);
      //     break;
      //   case MineType.CHASER:
      //     mine = new ChaserMine(size, this.item.position);
      //     break;
      //   case MineType.SHOOTER:
      //     mine = new ShooterMine(size, this.item.position, ship);
      //     break;
      case MineType.DRIFTER:
      default:
        mine = new DrifterMine(size, this.item.position);
        break;
    }
    this.state = SpawnState.INACTIVE;
    mine.spawn();
    return mine;
  }
}
