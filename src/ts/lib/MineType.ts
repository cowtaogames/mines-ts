export enum MineType {
  DRIFTER,
  SHOOTER,
  CHASER,
  CHASER_SHOOTER,
}
