import { MineType } from './MineType';

export interface Round {
  number: number;
  mineTypes: MineType[];
}
