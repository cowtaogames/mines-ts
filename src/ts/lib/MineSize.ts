export enum MineSize {
  SM = 0.4,
  MD = 0.7,
  LG = 1.2,
}
