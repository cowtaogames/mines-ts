export enum ActorState {
  SPAWNING,
  ACTIVE,
  DYING,
  DEAD,
  INACTIVE,
}
