export enum TextState {
  INACTIVE,
  FADING_IN,
  ACTIVE,
  FADING_OUT,
}
