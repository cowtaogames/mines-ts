export enum ExplosionSize {
  LG = 7,
  MD = 4,
  SM = 2,
}
