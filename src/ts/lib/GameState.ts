export enum GameState {
  INIT,
  ROUND_START,
  PLAYING,
  PAUSED,
  ROUND_END,
  GAME_OVER,
  IDLE,
}
