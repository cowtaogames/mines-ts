// @ts-nocheck
import paper from 'paper';
import { ExplosionSize, MineSize, MineType } from './lib';

interface Glyph {
  [index: string]: paper.Group;
}
export const MAIN_COLOR = '#d9feff';

export const swapColor = function (color) {
  if (color) {
    paper.project.currentStyle.strokeColor = color;
  } else {
    paper.project.currentStyle.strokeColor = MAIN_COLOR;
  }
};

export class AssetManager {
  ship: paper.Item[];

  static getShip(): paper.Item[] {
    if (this.ship) {
      return this.ship;
    } else {
      let body = new paper.Path([10, 0], [-8, -5], [-8, 5]);
      body.closed = true;
      swapColor('#e4baff');
      let wings = new paper.Path(
        [-2, 8],
        [-8, 6],
        [-8, -6],
        [-2, -8],
        [-10, -8],
        [-8, 0],
        [-10, 8]
      );
      wings.closed = true;
      swapColor('#ff8b26');
      let thrust = new paper.Path([-9, -2], [-15, 0], [-9, 2]);
      thrust.visible = false;
      swapColor();
      let ship = [new paper.Group(body, wings, thrust), thrust];
      ship[0].visible = false;
      this.ship = ship;
      return ship;
    }
  }

  static mine(type, size = MineSize.LG) {
    console.log('AssetManager mine', type);
    switch (type) {
      case MineType.DRIFTER:
      default:
        return this.drifter(size);
      // case MineType.SHOOTER:
      //   return this.shooter(size);
      // case MineType.CHASER:
      //   return this.chaser(size);
      // case MineType.CHASER_SHOOTER:
      //   return this.chaserShooter(size);
    }
  }

  static drifter(size) {
    console.log('AssetManager drifter');
    swapColor();
    let body = new paper.Path(
      [0, 25],
      [5, 5],
      [25, 0],
      [5, -5],
      [0, -25],
      [-5, -5],
      [-25, 0],
      [-5, 5]
    );
    body.closed = true;
    swapColor('#3d3dff');
    let charge = new paper.Path([10, 10], [10, -10], [-10, -10], [-10, 10]);
    charge.closed = true;
    let group = new paper.Group(body, charge);
    // scale based on size
    group.scale(size);
    swapColor();
    group.visible = false;
    return group;
  }

  //   static shooter(size) {
  //     console.log('AssetManager shooter');
  //     swapColor();
  //     let body = new paper.Path(
  //       [0, 25],
  //       [5, 5],
  //       [25, 0],
  //       [5, -5],
  //       [0, -25],
  //       [-5, -5],
  //       [-25, 0],
  //       [-5, 5]
  //     );
  //     body.closed = true;
  //     swapColor('yellow');
  //     let charge = new paper.Path([10, 10], [10, -10], [-10, -10], [-10, 10]);
  //     charge.closed = true;
  //     let group = new paper.Group(body, charge);
  //     // scale based on size
  //     group.scale(size);
  //     swapColor();
  //     group.visible = false;
  //     return group;
  //   }

  //   static chaser(size) {
  //     console.log('AssetManager chaser');
  //     swapColor();
  //     let body = new paper.Path(
  //       [0, 25],
  //       [5, 5],
  //       [25, 0],
  //       [5, -5],
  //       [0, -25],
  //       [-5, -5],
  //       [-25, 0],
  //       [-5, 5]
  //     );
  //     body.closed = true;
  //     swapColor('purple');
  //     let charge = new paper.Path([10, 10], [10, -10], [-10, -10], [-10, 10]);
  //     charge.closed = true;
  //     let group = new paper.Group(body, charge);
  //     // scale based on size
  //     group.scale(size);
  //     swapColor();
  //     group.visible = false;
  //     return group;
  //   }

  //   static chaserShooter(size) {
  //     console.log('AssetManager chaserShooter');
  //     swapColor();
  //     let body = new paper.Path(
  //       [0, 25],
  //       [5, 5],
  //       [25, 0],
  //       [5, -5],
  //       [0, -25],
  //       [-5, -5],
  //       [-25, 0],
  //       [-5, 5]
  //     );
  //     body.closed = true;
  //     swapColor('red');
  //     let charge = new paper.Path([10, 10], [10, -10], [-10, -10], [-10, 10]);
  //     charge.closed = true;
  //     let group = new paper.Group(body, charge);
  //     // scale based on size
  //     group.scale(size);
  //     swapColor();
  //     group.visible = false;
  //     return group;
  //   }

  static explosion(size: ExplosionSize) {
    console.log('AssetManager explosion', size);
    swapColor('red');
    let items = [];
    let circle = new paper.Path.Circle(new paper.Point(0, 0), 1);
    circle.fillColor = 'red';
    items.push(circle);
    let base = [
      [2, 10],
      [5, 6],
      [7, 2],
      [7, -3],
      [5, -7],
      [1, -10],
      [-4, -6],
      [-5, -2],
      [-7, 3],
      [-3, 5],
      [-1, 9],
    ];

    let points = base.map((p) => {
      return [p[0] * size, p[1] * size];
    });

    swapColor();
    for (let p of points) {
      let path = new paper.Path(p, [0, 0]);
      items.push(path);
    }

    let group = new paper.Group(items);
    group.visible = false;
    return group;
  }

  static spawner(): paper.Item {
    swapColor();
    let p1 = new paper.Path([1, 1], [-1, -1]);
    let p2 = new paper.Path([-1, 1], [1, -1]);
    let spawner = new paper.Group(p1, p2);
    spawner.visible = false;
    return spawner;
  }

  static fireball(): paper.Group {
    swapColor('#ff8b26');
    let p1 = new paper.Path([-3, 0], [3, 0]);
    let p2 = new paper.Path([0, 3], [0, -3]);
    swapColor('red');
    let p3 = new paper.Path([-3, 3], [3, -3]);
    swapColor('yellow');
    let p4 = new paper.Path([-3, -3], [3, 3]);
    let ball = new paper.Group(p1, p2, p3, p4);
    ball.scale(1.5);
    swapColor();
    ball.visible = false;
    return ball;
  }

  static deadShip(): paper.Item[] {
    swapColor();
    let nose = new paper.Path([10, 0], [3, -2], [2, -1], [4, 1], [3, 2]);
    nose.closed = true;
    nose.visible = false;
    let body = new paper.Path(
      [0, -3],
      [-4, -3],
      [-6, -2],
      [-5, 0],
      [-6, 2],
      [-5, 4],
      [3, 2],
      [4, 1],
      [2, -1],
      [3, -2]
    );
    body.closed = true;
    body.visible = false;
    let back = new paper.Path(
      [-5, 4],
      [-6, 2],
      [-5, 0],
      [-6, -2],
      [-4, -3],
      [0, -3],
      [-8, -5],
      [-8, 5]
    );
    back.closed = true;
    back.visible = false;
    swapColor('#e4baff');
    let wing = new paper.Path([-8, 0], [-8, -7], [-4, -8], [-10, -8]);
    wing.closed = true;
    wing.visible = false;
    let wingtip = new paper.Path([-4, 5], [-8, 8], [-8, 7]);
    wingtip.closed = true;
    wingtip.visible = false;
    swapColor();
    return [nose, body, back, wing, wingtip];
  }

  static glyphs() {
    let space = new paper.Group([new paper.Path([1, -2], [-1, 2])]);
    space.data = {
      noRender: true,
    };
    let glyphs = {
      // A
      A: new paper.Group([
        new paper.Path([-1, 2], [-1, -1], [0, -2], [1, -1], [1, 2]),
        new paper.Path([-1, 0], [1, 1]),
      ]),
      // B
      B: new paper.Group([
        new paper.Path(
          [-1, 2],
          [-1, -2],
          [1, -1],
          [0, 0],
          [1, 1],
          [0, 2],
          [-1, 2]
        ),
      ]),
      C: new paper.Group([
        new paper.Path([1, 1], [0, 2], [-1, 1], [-1, -1], [0, -2], [1, -1]),
      ]),
      D: new paper.Group([
        new paper.Path(
          [-1, 2],
          [-1, -2],
          [0, -2],
          [1, -1],
          [1, 1],
          [0, 2],
          [-1, 2]
        ),
      ]),
      E: new paper.Group([
        new paper.Path([1, 2], [-1, 2], [-1, -2], [1, -2]),
        new paper.Path([-1, -1], [0, -1]),
      ]),
      F: new paper.Group([
        new paper.Path([-1, 2], [-1, -2], [1, -2]),
        new paper.Path([
          [-1, 0],
          [0, 0],
        ]),
      ]),
      G: new paper.Group([
        new paper.Path(
          [0, 0],
          [1, 0],
          [1, 2],
          [0, 2],
          [-1, 1],
          [-1, -1],
          [0, -2],
          [1, -1]
        ),
      ]),
      H: new paper.Group([
        new paper.Path([-1, 2], [-1, -2]),
        new paper.Path([1, 2], [1, -2]),
        new paper.Path([-1, 0], [1, 1]),
      ]),
      I: new paper.Group([
        new paper.Path([-1, 2], [1, 2]),
        new paper.Path([0, 2], [0, -2]),
        new paper.Path([-1, -2], [1, -2]),
      ]),
      J: new paper.Group([
        new paper.Path([1, -2], [1, 1], [0, 2], [-1, 1], [-1, 0]),
      ]),
      K: new paper.Group([
        new paper.Path([-1, 2], [-1, -2]),
        new paper.Path([1, -2], [1, -2], [-1, 1]),
        new paper.Path([0, 0], [1, 1], [1, 2]),
      ]),
      L: new paper.Group([new paper.Path([-1, -2], [-1, 2], [1, 2])]),
      M: new paper.Group([
        new paper.Path([-1, 2], [-1, -2], [0, -1], [1, -2], [1, 2]),
      ]),
      N: new paper.Group([
        new paper.Path([-1, 2], [-1, -2], [0, -1], [0, 1], [1, 2], [1, -2]),
      ]),
      O: new paper.Group([
        new paper.Path(
          [1, 1],
          [0, 2],
          [-1, 2],
          [-1, -1],
          [0, -2],
          [1, -2],
          [1, 1]
        ),
      ]),
      P: new paper.Group([
        new paper.Path([-1, 2], [-1, -2], [0, -2], [1, -1], [-1, 1]),
      ]),
      Q: new paper.Group([
        new paper.Path(
          [1, 1],
          [0, 2],
          [-1, 2],
          [-1, -1],
          [0, -2],
          [1, -2],
          [1, 1]
        ),
        new paper.Path([0, 1], [1, 2]),
      ]),
      R: new paper.Group([
        new paper.Path([-1, 2], [-1, -2], [0, -2], [1, -1], [-1, 1]),
        new paper.Path([0, 0], [1, 1], [1, 2]),
      ]),
      S: new paper.Group([
        new paper.Path(
          [1, -1],
          [0, -2],
          [-1, -1],
          [1, 0],
          [1, 1],
          [0, 2],
          [-1, 1]
        ),
      ]),
      T: new paper.Group([
        new paper.Path([0, 2], [0, -2]),
        new paper.Path([-1, -2], [1, -2]),
      ]),
      U: new paper.Group([
        new paper.Path([-1, -2], [-1, 2], [0, 2], [1, 1], [1, -2]),
      ]),
      V: new paper.Group([
        new paper.Path([-1, -2], [-1, -1], [0, 2], [1, 0], [1, -2]),
      ]),
      W: new paper.Group([
        new paper.Path([-1, -2], [-1, 1], [0, 2], [1, 2], [1, -2]),
        new paper.Path([0, 0], [0, 2]),
      ]),
      X: new paper.Group([
        new paper.Path([-1, -2], [-1, -1], [1, 1], [1, 2]),
        new paper.Path([1, -2], [1, -1], [-1, 1], [-1, 2]),
      ]),
      Y: new paper.Group([
        new paper.Path([-1, -2], [-1, 0], [0, 0], [0, 2]),
        new paper.Path([1, -2], [1, -1], [0, 0]),
      ]),
      Z: new paper.Group([new paper.Path([-1, -2], [1, -2], [-1, 2], [1, 2])]),
      space,
      0: new paper.Group([
        new paper.Path(
          [1, 1],
          [0, 2],
          [-1, 2],
          [-1, -1],
          [0, -2],
          [1, -2],
          [1, 1]
        ),
        new paper.Path([0, -1], [0, 1]),
      ]),
      1: new paper.Group([
        new paper.Path([-1, -1], [0, -2], [0, 2]),
        new paper.Path([-1, 2], [1, 2]),
      ]),
      2: new paper.Group([
        new paper.Path([-1, -1], [-1, -2], [1, -2], [1, 0], [-1, 2], [1, 2]),
      ]),
      3: new paper.Group([
        new paper.Path([-1, -2], [1, -2], [1, 1], [0, 2], [-1, 2]),
        new paper.Path([-1, -1], [1, -1]),
      ]),
      4: new paper.Group([
        new paper.Path([-1, -2], [-1, 1], [1, 1]),
        new paper.Path([1, -2], [1, 2]),
      ]),
      5: new paper.Group([
        new paper.Path(
          [1, -2],
          [-1, -2],
          [-1, -1],
          [1, 0],
          [1, 2],
          [0, 2],
          [-1, 1]
        ),
      ]),
      6: new paper.Group([
        new paper.Path([-1, -2], [-1, 2], [1, 2], [1, 0], [0, 0], [-1, 1]),
      ]),
      7: new paper.Group([
        new paper.Path([-1, -1], [-1, -2], [1, -2], [1, -1], [-1, 1], [-1, 2]),
      ]),
      8: new paper.Group([
        new paper.Path(
          [-1, -2],
          [0, -2],
          [1, -1],
          [1, 2],
          [0, 2],
          [-1, 1],
          [-1, -2]
        ),
        new paper.Path([-1, 0], [1, 0]),
      ]),
      9: new paper.Group([
        new paper.Path([1, 0], [-1, 0], [-1, -2], [0, -2], [1, -1], [1, 2]),
      ]),
    };
    return glyphs;
  }
}
