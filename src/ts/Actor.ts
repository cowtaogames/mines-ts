import paper from 'paper';
import { ActorState } from './lib';

export class Actor {
  angle: number;
  item: paper.Item;
  points: number;
  position: paper.Point;
  rotation: number;
  speed: number;
  state: ActorState;
  vector: paper.Point;

  activate(): void {}
  die(ship?: any): void {}
  update(): void {}

  keepInView(item?: paper.Item) {
    let position = item ? item.position : this.item.position;
    let bounds = paper.view.bounds;
    if (position.x > bounds.width) {
      position.x = 0;
    }
    if (position.x < 0) {
      position.x = bounds.width;
    }
    if (position.y > paper.view.size.height) {
      position.y = 0;
    }
    if (position.y < 0) {
      position.y = bounds.height;
    }
  }
}
