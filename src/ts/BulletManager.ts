import paper from 'paper';
import { Actor } from './Actor';
import { AssetManager } from './AssetManager';
import { BulletManagerType } from './lib';

export class BulletManager extends Actor {
  bullets: paper.Group;
  children: paper.Item[];
  ttl: number;
  max: number;
  type: BulletManagerType;

  constructor(type: BulletManagerType) {
    super();
    this.bullets = new paper.Group();
    this.children = this.bullets.children;
    this.ttl = 60;
    this.max = 5;
    this.type = type || BulletManagerType.SHIP;
  }

  shoot(pos: paper.Point, angle: number) {
    // console.log('BulletManager shoot', pos, angle);
    // 5 bullets max at any given time
    if (this.children.length == this.max) {
      return;
    }
    let vector = new paper.Point({ angle, length: 9 });
    let center = new paper.Point(pos.x, pos.y);
    center.x += vector.length;
    center.y += vector.length;
    let bullet = new paper.Path.Circle({
      center,
      radius: 0.5,
      fillColor: '#ffffff',
      strokeWidth: 0,
      data: {
        vector,
        timeToDie: 58,
      },
    });
    this.bullets.addChild(bullet);
  }

  shootAt(from: paper.Point, to: paper.Point) {
    let bullet = AssetManager.fireball();
    bullet.position = from;
    let angle = Math.floor(Math.random() * Math.floor(359));
    if (to) {
      angle = (Math.atan2(to.y - from.y, to.x - from.x) * 180) / Math.PI;
    }
    let vector = new paper.Point({
      angle,
      length: 3,
    });
    bullet.data = {
      timeToDie: 200,
      vector,
    };
    bullet.visible = true;
    console.log('BulletManager shootAt', angle);
    this.bullets.addChild(bullet);
  }

  kill(fireball: paper.Group) {
    fireball.remove();
  }

  update() {
    for (let bullet of this.bullets.children) {
      bullet.data.timeToDie--;
      if (bullet.data.timeToDie < 1) {
        bullet.remove();
      } else {
        bullet.position.x += bullet.data.vector.x;
        bullet.position.y += bullet.data.vector.y;
        if (this.type === BulletManagerType.MINE) {
          bullet.rotate(50);
        }
        this.keepInView(bullet);
      }
    }
  }
}
