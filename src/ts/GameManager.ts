import paper from 'paper';

import {
  ActorState,
  BulletManagerType,
  GameState,
  MineSize,
  MineType,
  Round,
} from './lib';
import { SpawnManager } from './SpawnManager';
import { Ship } from './Ship';
import { BulletManager } from './BulletManager';
import { Billboard } from './Billboard';
import { Timer } from './Timer';
import { Actor } from './Actor';
import { MineBase } from './MineBase';

export function timer(ms: number) {
  return new Promise((res) => setTimeout(res, ms));
}

export class GameManager {
  state: GameState;
  score: number;
  firstExtraShip: number;
  extraShipInterval: number;
  timer: Timer;
  ship: Ship;
  bulletManager: BulletManager;
  mineBulletManager: BulletManager;
  spawnManager: SpawnManager;
  gameInfo: Billboard;
  scoreBoard: Billboard;
  mines: MineBase[];

  constructor() {
    console.log('GameManager constructor');
    this.state = GameState.INIT;
    this.score = 0;
    this.firstExtraShip = 10000;
    this.extraShipInterval = 20000;
    this.timer = new Timer();
    this.ship = new Ship();
    this.spawnManager = new SpawnManager();
    this.gameInfo = new Billboard(paper.view.center, 5);
    this.scoreBoard = new Billboard(
      new paper.Point(paper.view.center.x, 15),
      5
    );
    this.scoreBoard.setText(this.score.toString());
    this.scoreBoard.activate();
  }

  initGame() {
    console.log('GameManager.initGame', this.timer.now());
    let round: Round = {
      number: 1,
      mineTypes: [
        MineType.DRIFTER,
        MineType.DRIFTER,
        MineType.DRIFTER,
        MineType.DRIFTER,
      ],
    };
    this.bulletManager = new BulletManager(BulletManagerType.SHIP);
    this.mineBulletManager = new BulletManager(BulletManagerType.MINE);
    this.spawnManager.setupSpawners(round);
  }

  async startGame() {
    console.log('GameManager.startGame', this.timer.now());
    this.state = GameState.PLAYING;
    this.ship.spawn();
    this.mines = this.spawnManager.spawnRound(this.ship);
  }

  gameOver() {
    this.state = GameState.GAME_OVER;
    this.ship.hide();
  }

  pause() {
    if (this.state === GameState.PLAYING) {
      this.state = GameState.PAUSED;
    } else if (this.state === GameState.PAUSED) {
      this.state = GameState.PLAYING;
    }
  }

  update() {
    this.gameInfo.update();
    this.scoreBoard.updateText(this.score.toString());
    if (this.state === GameState.PLAYING) {
      // update ship
      this.ship.update();
      for (let mine of this.mines) {
        mine.update();
        switch (mine.state) {
          case ActorState.ACTIVE: {
            if (this.ship.state === ActorState.ACTIVE) {
              // check collisions
              if (mine.item.bounds.intersects(this.ship.item.bounds)) {
                mine.die(this.ship);
                this.score += mine.points;
                this.spawnMoreMines(mine);
                this.mineShoot(mine);
                this.ship.die();
              }
            }
          }
        }
      }
      // move mines
      // for (let mine of this.mines) {
      //   switch (mine.state) {
      //     case ActorState.ACTIVE:
      //       if (this.ship.state !== ActorState.ACTIVE) {
      //         mine.update(null);
      //       } else {
      //         mine.update(this.ship.item.position);
      //         //check collision with ship
      //         if (this.ship.state === ActorState.ACTIVE) {
      //           if (mine.item.bounds.intersects(this.ship.item.bounds)) {
      //             mine.die(this.ship);
      //             this.score += mine.points;
      //             this.spawnMoreMines(mine);
      //             this.mineShoot(mine);
      //             this.ship.die();
      //           }
      //         }
      //       }
      //     case ActorState.DYING:
      //       if (this.ship.state !== ActorState.ACTIVE) {
      //         mine.update(null);
      //       } else {
      //         mine.update(this.ship.item.position);
      //       }
      //       break;
      //     case ActorState.DEAD:
      //       // remove it from the list:
      //       mine.item.visible = false;
      //       this.mines = this.mines.filter((m) => m.item.id !== mine.item.id);
      //       break;
      //     case ActorState.INACTIVE:
      //       let delta = Math.floor(this.timer.now());
      //       if (delta - mine.spawned > 800) {
      //         mine.activate();
      //       }
      //   }
      // }

      // update bullets
      let bm = this.bulletManager;
      bm.update();
      // if (bm.type === BM_TYPE.SHIP) {
      // check collisions
      for (let bullet of bm.children) {
        // bullets vs mines
        for (let mine of this.mines) {
          if (mine.state === ActorState.ACTIVE) {
            if (mine.item.bounds.contains(bullet.position)) {
              // kill mine
              mine.die(this.ship);
              this.score += mine.points;
              this.spawnMoreMines(mine);
              this.mineShoot(mine);
              // kill bullet
              this.bulletManager.die(bullet);
              continue;
            }
          }
        }
        // bullets vs fireballs
        // for (let fireball of this.mineBulletManager.children) {
        //   if (fireball.bounds.contains(bullet.position)) {
        //     this.mineBulletManager.die(fireball);
        //     this.score += 150;
        //   }
        // }
      }
      // }

      //update fireballs
      // this.mineBulletManager.update();
      // for (let fireball of this.mineBulletManager.children) {
      //   if (this.ship.item.bounds.intersects(fireball.bounds)) {
      //     // remove fireball
      //     fireball.data.ttd = 0;
      //     this.ship.die();
      //   }
      // }
    }
  }

  spawnMoreMines(mine: MineBase) {
    // if it was LG or MD, spawn 2 of the next smallest size
    console.log('spawnMoreMines');
    if (mine.size === MineSize.LG || mine.size === MineSize.MD) {
      let size = mine.size === MineSize.LG ? MineSize.MD : MineSize.SM;
      let newMines = this.spawnManager.spawn(size, mine.type, 2, this.ship);
      this.mines = [...this.mines, ...newMines];
    }
  }

  mineShoot(mine: MineBase) {
    // if it was a shooter type, shoot a fireball at the player
    if (
      mine.type === MineType.SHOOTER ||
      mine.type === MineType.CHASER_SHOOTER
    ) {
      // this.mineBulletManager.shootAt(
      //   mine.item.position,
      //   this.ship.item.position
      // );
    }
  }

  playerFire() {
    if (this.ship.state === ActorState.ACTIVE) {
      this.bulletManager.shoot(this.ship.item.position, this.ship.angle);
    }
  }

  shipRespawn() {
    console.log('GameManager shipRespawn', this.ship.state);
    if (this.ship.state === ActorState.INACTIVE) {
      this.ship.spawn();
    }
  }
}
