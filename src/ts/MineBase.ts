import paper from 'paper';
import { Actor } from './Actor';
import { ActorState, ExplosionSize, MineSize, MineType } from './lib';
import { Timer } from './Timer';
import { Ship } from './Ship';
import { AssetManager } from './AssetManager';

export class MineBase extends Actor {
  explosion: paper.Item;
  size: MineSize;
  spawnDelay: number;
  spawnTime: number;
  timeOfDeath: number;
  timer: Timer;
  ttd: number;
  type: MineType;

  constructor(size: MineSize, position: paper.Point) {
    console.log('MineBase constructor', size, position);
    super();
    this.size = size;
    this.rotation = 0;
    this.points = this.getPoints();
    this.position = position;
    this.state = ActorState.INACTIVE;
    this.spawnDelay = 800;
    this.timer = new Timer();
    this.angle = Math.floor(Math.random() * Math.floor(359));
    this.explosion = AssetManager.explosion(this.getExpSize());
    this.vector = new paper.Point({ angle: this.angle, length: 1 });
  }

  activate() {
    console.log('MineBase activate');
    this.state = ActorState.ACTIVE;
    this.vector.length = 1;
  }

  spawn() {
    console.log('MineBase spawn');
    this.item.visible = true;
    this.item.position = this.position;
    this.state = ActorState.SPAWNING;
    this.vector.length = 0;
    this.spawnTime = Math.floor(this.timer.now());
  }

  update() {
    let delta = Math.floor(this.timer.now());
    switch (this.state) {
      case ActorState.SPAWNING: {
        if (delta - this.spawnTime > this.spawnDelay) {
          this.activate();
        }
        break;
      }
      case ActorState.ACTIVE:
        this.item.position.x = this.item.position.x + this.vector.x;
        this.item.position.y = this.item.position.y + this.vector.y;
        this.item.rotate(this.rotation);
        this.keepInView();
        break;
      case ActorState.DYING:
        this.explosion.opacity -= 0.01;
        if (delta - this.timeOfDeath > this.ttd) {
          this.state = ActorState.DEAD;
          this.explosion.visible = false;
        }
        break;
    }
  }

  die(ship?: Ship) {
    this.state = ActorState.DYING;
    this.vector.length = 0;
    this.item.visible = false;
    this.explosion.position = this.item.position;
    this.explosion.visible = true;
    this.timeOfDeath = Math.floor(this.timer.now());
  }

  stop() {
    this.vector.length = 0;
  }

  start() {
    this.vector.length = 1;
  }

  getExpSize() {
    console.log('MineBase getExpSize', this.size);
    switch (this.size) {
      case MineSize.LG:
        return ExplosionSize.LG;
      case MineSize.MD:
        return ExplosionSize.MD;
      case MineSize.SM:
        return ExplosionSize.SM;
    }
  }

  getPoints(): number {
    console.log('MineBase getPoints', this.size);
    switch (this.size) {
      case MineSize.LG:
        return 50;
      case MineSize.MD:
        return 100;
      case MineSize.SM:
        return 250;
    }
  }
}
