import paper from 'paper';
import { TextState } from './lib';
import { AssetManager } from './AssetManager';

export class Billboard {
  position: paper.Point;
  glyphs: any;
  text: string;
  state: TextState;
  fadeTime: number;
  time: number;
  size: number;
  item: paper.Group;

  constructor(pos: paper.Point, size: number) {
    console.log('TextDisplay constructor', pos, size);
    this.position = pos;
    this.glyphs = AssetManager.glyphs();
    this.text = '';
    this.state = TextState.INACTIVE;
    this.fadeTime = 100;
    this.time = 0;
    this.size = size || 5;
  }

  init() {
    for (let glyph of this.glyphs) {
      glyph.pivot = new paper.Point(-1, -2);
      // glyph.visible = false;
    }
  }

  update() {
    switch (this.state) {
      case TextState.FADING_IN:
        this.time++;
        let inpacity = 1 - (this.fadeTime - this.time) / this.fadeTime;
        if (inpacity > 1) {
          inpacity = 1;
          this.state = TextState.ACTIVE;
        }
        this.item.opacity = inpacity;
        break;
      case TextState.FADING_OUT:
        this.time--;
        let outpacity = 1 - (this.fadeTime - this.time) / this.fadeTime;
        if (outpacity < 0) {
          outpacity = 0;
          this.state = TextState.INACTIVE;
        }
        this.item.opacity = outpacity;
    }
  }

  show() {
    console.log('TextDisplay show');
    this.state = TextState.FADING_IN;
    this.time = 0;
  }

  activate() {
    console.log('TextDisplay activate');
    this.state = TextState.ACTIVE;
    this.item.opacity = 1;
  }

  fade() {
    console.log('TextDisplay fade');
    this.state = TextState.FADING_OUT;
    this.time = this.fadeTime;
  }

  center() {}

  setText(text: string) {
    let glyphs = this.getGlyphs(text);
    let group = [];
    for (let i = 0; i < glyphs.length; i++) {
      let offset = i * 2.8;
      glyphs[i].visible = true;
      glyphs[i].position.x += offset;
      glyphs[i].shadowColor = '#ffffff';
      glyphs[i].shadowBlur = 10;
      glyphs[i].opacity = 1;
      if (glyphs[i].data && glyphs[i].data.noRender) {
        glyphs[i].visible = false;
      }
      group.push(glyphs[i]);
    }
    this.item = new paper.Group(group);
    this.item.scale(this.size);
    this.item.position = this.position;
    this.item.opacity = 0;
  }

  updateText(text: string) {
    let opacity = this.item.opacity;
    this.item.removeChildren();
    let glyphs = this.getGlyphs(text);
    let group = [];
    for (let i = 0; i < glyphs.length; i++) {
      let offset = i * 2.8;
      glyphs[i].visible = true;
      glyphs[i].position.x += offset;
      glyphs[i].shadowColor = '#ffffff';
      glyphs[i].shadowBlur = 10;
      glyphs[i].opacity = 1;
      if (glyphs[i].data && glyphs[i].data.noRender) {
        glyphs[i].visible = false;
      }
      group.push(glyphs[i]);
    }
    this.item.addChildren(group);
    this.item.opacity = opacity;
    this.item.scale(this.size);
    this.item.position = this.position;
  }

  getGlyphs(input: string) {
    let raw = input.split('');
    let glyphOut = [];
    for (let char of raw) {
      char = char.toUpperCase();
      if (this.glyphs[char]) {
        glyphOut.push(this.glyphs[char].clone());
      } else if (char === ' ') {
        glyphOut.push(this.glyphs.space.clone());
      }
    }
    return glyphOut;
  }
}
