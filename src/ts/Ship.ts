import paper from 'paper';
import { Actor } from './Actor';
import { AssetManager } from './AssetManager';
import { ActorState } from './lib';

export class Ship extends Actor {
  thrustItem: paper.Item;
  parts: paper.Item[];
  ttd: number;
  spinSpeed: number;

  constructor(position: paper.Point = paper.view.bounds.center) {
    super();
    [this.item, this.thrustItem] = AssetManager.getShip();
    this.item.position = position || paper.view.bounds.center;
    this.item.pivot = this.item.bounds.center;
    this.item.visible = false;
    this.item.applyMatrix = false;
    this.angle = 0;
    this.vector = new paper.Point({
      angle: this.angle,
      length: 1,
    });
    this.speed = 0.2;
    this.parts = AssetManager.deadShip();
    this.state = ActorState.INACTIVE;
    this.ttd = 40;
    this.spinSpeed = 5;
    // this.initExplosion();
  }

  spawn() {
    this.state = ActorState.SPAWNING;
    this.item.position = paper.view.bounds.center;
    this.angle = 0;
    this.vector = new paper.Point({
      angle: this.angle,
      length: 0,
    });
    this.speed = 0.2;
    this.item.rotation = 0;
    this.item.rotate(-90);
    this.angle -= 90;
    this.item.visible = true;
    this.ttd = 40;
    this.state = ActorState.ACTIVE;
  }

  // movement
  turnLeft() {
    if (this.state === ActorState.ACTIVE) {
      this.item.rotate(-this.spinSpeed);
      this.angle -= this.spinSpeed;
    }
  }

  turnRight() {
    if (this.state === ActorState.ACTIVE) {
      this.item.rotate(this.spinSpeed);
      this.angle += this.spinSpeed;
    }
  }

  thrust() {
    if (this.state === ActorState.ACTIVE) {
      this.thrustItem.visible = true;
      let delta = new paper.Point({
        angle: this.angle,
        length: this.speed,
      });
      this.vector.x += delta.x;
      this.vector.y += delta.y;
      if (this.vector.length > 8) {
        this.vector.length = 8;
      }
    }
  }

  coast() {
    this.thrustItem.visible = false;
    this.vector.length *= 0.9;
  }

  hide() {
    this.state = ActorState.INACTIVE;
    this.item.visible = false;
  }

  update() {
    switch (this.state) {
      case ActorState.ACTIVE:
        this.item.position.x += this.vector.x;
        this.item.position.y += this.vector.y;
        this.keepInView();
        break;
      case ActorState.DYING:
        this.ttd--;
        if (this.ttd < 0) {
          this.dead();
        }
        for (let part of this.parts) {
          // @ts-ignore
          part.position.x += part.vector.x;
          // @ts-ignore
          part.position.y += part.vector.y;
          part.opacity -= 1 / this.ttd;
          part.rotate(Math.floor(Math.random() * 20));
        }
        break;
    }
  }

  stop() {
    this.vector.length = 0;
  }

  dead() {
    this.state = ActorState.DEAD;
    this.state = ActorState.INACTIVE;
  }

  die() {
    // this.stop();
    this.item.visible = false;
    this.state = ActorState.DYING;
    for (let part of this.parts) {
      part.position = this.item.position;
      let angle = Math.floor(Math.random() * Math.floor(359));
      // @ts-ignore
      part.vector = new paper.Point({ angle, length: 5 });
      part.visible = true;
      part.opacity = 1;
    }
  }
}
