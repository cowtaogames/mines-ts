import paper from 'paper';
import { Spawner } from './Spawner';
import { MineSize, MineType, Round } from './lib';
import { Ship } from './Ship';
import { MineBase } from './MineBase';

export class SpawnManager {
  spawners: Spawner[];
  maxX: number;
  maxY: number;
  minX: number;
  minY: number;
  currentRound: Round;

  constructor() {
    // this.roundNumber = 1;
    this.spawners = [];
    this.setBoundaries();
  }

  setBoundaries() {
    // set some boundaries, so as to not spawn
    //   within 10% of the view size from the edges
    let viewX = paper.view.size.width;
    let viewY = paper.view.size.height;
    this.maxX = viewX - 0.1 * viewX;
    this.maxY = viewY - 0.1 * viewY;
    this.minX = 0.1 * viewX;
    this.minY = 0.1 * viewY;
  }

  setupSpawners(round: Round) {
    console.log('SpawnManager setupSpawners');
    this.currentRound = round;
    let count = round.mineTypes.length;
    let total = count + count * 2 + count * 4;
    for (let l = 0; l < total; l++) {
      let posX = Math.floor(
        Math.random() * (this.maxX - this.minX + 1) + this.minX
      );
      let posY = Math.floor(
        Math.random() * (this.maxY - this.minY + 1) + this.minY
      );
      let spawner = new Spawner(new paper.Point(posX, posY));
      this.spawners.push(spawner);
    }
  }

  spawnRound(ship: Ship): MineBase[] {
    console.log('SpawnManager spawnRound');
    let mines: MineBase[] = [];
    for (let type of this.currentRound.mineTypes) {
      mines = [...mines, ...this.spawn(MineSize.LG, type, 1, ship)];
    }
    return mines;
  }

  spawn(
    size: MineSize,
    type: MineType,
    count: number = 2,
    ship: Ship
  ): MineBase[] {
    console.log('SpawnManager spawn');
    let spawned = [];
    for (let i = 0; i < count; i++) {
      let spawner = this.spawners.pop();
      if (spawner) {
        spawned.push(spawner.spawn(type, size, ship));
      }
    }
    return spawned;
  }
}
