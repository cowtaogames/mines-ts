export class Timer {
  now() {
    if (window.performance.now) {
      return window.performance.now();
    } else {
      return new Date().getTime();
    }
  }
}
