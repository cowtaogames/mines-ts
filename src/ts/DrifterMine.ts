import paper from 'paper';
import { AssetManager } from './AssetManager';
import { MineSize, MineType } from './lib';
import { MineBase } from './MineBase';

export class DrifterMine extends MineBase {
  constructor(size: MineSize, position: paper.Point) {
    console.log('DrifterMine constructor', size, position);
    super(size, position);
    this.item = AssetManager.mine(MineType.DRIFTER, size);
    this.item.visible = false;
    this.item.position = position;
  }
}
