import './css/index.css';

import paper from 'paper';
import { GameManager } from './ts/GameManager';

window.onload = function () {
  const canvas = document.getElementById('canvas') as HTMLCanvasElement;
  paper.setup(canvas);
  paper.project.currentStyle.strokeColor = new paper.Color(0.85, 0.996, 1, 0.8);

  // load GM
  const gameManager = new GameManager();

  // setup event handlers
  // paper's onFrame handler
  paper.view.onFrame = function () {
    gameManager.update();
    if (paper.Key.isDown('left')) {
      gameManager.ship.turnLeft();
    }
    if (paper.Key.isDown('right')) {
      gameManager.ship.turnRight();
    }
    if (paper.Key.isDown('up')) {
      gameManager.ship.thrust();
    } else {
      gameManager.ship.coast();
    }
  };

  // INPUT handling
  document.onkeyup = function (event) {
    if (event.key == 'z') {
      gameManager.playerFire();
    }
    if (event.key == '/') {
      gameManager.shipRespawn();
    }
    if (event.key == ' ') {
      gameManager.pause();
    }
  };

  // Stop left and right keyboard events from propagating.
  document.onkeydown = function (event) {
    if (event.key == 'left' || event.key == 'right') {
      return false;
    }
  };

  gameManager.initGame();
  gameManager.startGame();
};
